//Jake Yeagley, Lab 09, CS 1415
#include <iostream>
#include "yeagPoint.h"									//including the .h file

using namespace std;

namespace Yeagley										//adding to the namespace
{
	point::point(int x,int y)							//constructor for the point
	{
		x_ = x;
		y_ = y;
	}
	int point::getx()const								//get functions
	{
		return x_;
	}

	int point::gety()const
	{
		return y_;
	}

	void point::setx(int x)								//set functions
	{
		x_ = x;
	}

	void point::sety(int y)
	{
		y_ = y;
	}
														//overloading the output operator
	ostream &operator << (ostream &out, const point &obj)
	{
		out << "(" << obj.x_ << "," << obj.y_ << ")";
		return out;
	}
}

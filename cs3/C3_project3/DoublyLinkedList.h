#include <vector>
#include <iostream>
#include <string>

//taken from the worksheet given in class

#ifndef DoublyLinkedList_H
#define DoublyLinkedList_H

class Node
{
public:
std::vector<std::string> value;
Node * next;
Node * prev;
Node(std::vector<std::string> v, Node * anext = NULL, Node *aprev = NULL) : value(v), next(anext), prev(aprev) {}
};

class DoublyLinkedList
{
int size;
Node* head;
Node* tail;

public:
DoublyLinkedList(void) { head = NULL; size = 0; tail = NULL; };
void add(std::vector<std::string>);
std::vector<std::string>get(int count);
std::vector<std::string>pop_back();
//void clear();
bool isEmpty(){ return head == NULL; };
std::string toString();
int getSize() { return size; };
};

#endif 
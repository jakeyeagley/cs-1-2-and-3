//Jake Yeagley, Lab 10, CS 1415
#ifndef TRIANGLE_H									//including safe gaurds
#define TRIANGLE_H

#include <iostream>						
#include "yeagShape.h"								//including the shape file

using namespace std;
	
namespace Yeagley									//adding to the namespace
{
	class triangle: public shape					//inheritating from the shape
	{
	private:										//3 data members for 3 sides of triangle
		double sideA_;
		double sideB_;
		double sideH_;

	public:											//constructor
		triangle(double a = 0, double b = 0, double h = 0);	
		void setSides(double,double,double);		//set function
		double getArea()const;						//get functions
		double getPerimeter()const;
		void print()const;							//print function

	};
}
#endif
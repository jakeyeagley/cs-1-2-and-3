#include "WordLadderState.h"

void wordLadderState::push_back(std::string word)
{
	ladder.push_back(word);
	size++;
	setScore();
}
void wordLadderState::setScore()
{ 
  lettersLeft = 0;
	for (int i = 0; i < lastWord.size(); i++)
	{
		if (ladder[ladder.size() - 1][i] != lastWord[i])
			lettersLeft++;
	}
	score = size + lettersLeft;
}

std::string wordLadderState::toString()
{
  std::ostringstream oss;
  oss << " [";
  for (int i = 0; i < ladder.size(); i++)
  {
    oss << ladder[i];
    if (i != ladder.size() - 1)
      oss << ",";
  }
  oss << "] " << score;
  return oss.str();
}

std::ostream& operator<<(std::ostream& ss, wordLadderState & gs)
{
  ss << gs.toString() << std::endl;
  return ss;
}
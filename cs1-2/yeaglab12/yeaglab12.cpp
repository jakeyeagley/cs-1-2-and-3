//Jake yeagley
//CS 1415
//this program uses a class template to store an array
#include "yeagarray.h"						//including the .h file
#include <string>							//including the string library

using namespace Yeagley;					//using the namespaces
using namespace std;

void add(Array<string>&);					//prototyping

int main()
{
	int size;								//variables
	int menu;
	string thing;
											//prompting
	cout << "this program will ask you to store items in an array" << endl;
	cout << "what size would you like your array to be?" << endl;
	cin >> size;

	Array<string> stuff(size);				//array variable
											//loop to get data
	for(int i = 0; i < stuff.getSize(); i++)
	{
		cout << "enter a value for element " << i << endl;
		cin >> stuff.at(i);
	}
											//prompting for menu
	cout << "if you would like to add something to the array press 1 " << endl;
	cout << "if you would like to remove something from the array press 2 " << endl;
	cout << "if you would like to look at a certain element of the array press 3 " << endl;
	cin >> menu;

	try										//try block
	{
		do									//loop for menu
		{

			if(menu == 1)					//1 is add 
			{
				add(stuff);
			}
			else if(menu == 2)				//2 is remove
			{
				cout << "the last element has been removed " << endl;
				stuff.remove();
			}
			else if(menu == 3)				//3 is display
			{
				for(int i = 0; i < stuff.getSize(); i++)
				cout << stuff.at(i) << endl;
			}
			else							//throw if they put a wrong number
				throw arrayException("error: value not known");
											//reprompting
			cout << "if you would like to add something to the array press 1 " << endl;
			cout << "if you would like to remove something from the array press 2 " << endl;
			cout << "if you would like to look at a certain element of the array press 3 " << endl;
			cout << "if you want to quit press 0" << endl;
			cin >> menu;					//input for menu
		}while(menu != 0);
	}
	catch(arrayException e)					//catch block
		{
			cout << e.what() << endl;		
		};

	return 0;
}

void add(Array<string>& stuff)
{ 
	string thing;							//creating string variable and prompting
	cout << "what would you like to add? " << endl;
	cin >> thing;
	stuff.add(thing);						//adding
}
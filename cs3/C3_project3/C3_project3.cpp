#include <iostream>
#include "WordLadderState.h"
#include "AVLtree.h"
#include "DoublyLinkedList.h"
#include <vector>
#include <fstream>
/*
	project 3
	this program show the difference from a double linked list and a AVL tree

	bruteGame: is the game from program 1 
	aGame: is the new game with an AVL tree
*/
void bruteGame(std::string inputWord, std::string finalWord);
void aGame(std::string inputWord, std::string finalWord);
std::vector<std::string>openfile(int sizeOfWord);
bool checkDictionaryAndMark(std::string inputWord, int end, int beg, std::vector<std::string> &dictionary, std::string finalWord);
bool checkDictionary(std::string inputWord, int end, int beg, std::vector<std::string> &dictionary, std::string finalWord);
bool checkWord(std::vector<std::string>ladder, std::string finalWord);
std::vector<std::string> changeLetterAndMark(std::string input, int sizeOfDictionary, std::vector<std::string>&dictionary, std::string finalWord);
std::vector<std::string> changeLetter(std::string input, int sizeOfDictionary, std::vector<std::string>&dictionary, std::string finalWord);

int main()
{ 
	AvlTree<int> testTree;
	testTree.insert(0);
	testTree.insert(1);
	testTree.insert(3);
	testTree.insert(5);
	testTree.insert(9);
	testTree.insert(9);
	testTree.insert(9);
	testTree.insert(11);
	testTree.insert(2);
	testTree.insert(9);
	testTree.insert(4);
	testTree.insert(8);
	cout << testTree.toString("message") << endl;

	testTree.remove(7);
	testTree.remove(9);
	cout << testTree.toString("message") << endl;

	testTree.removeMin();
	cout << testTree.toString("message") << endl;

	testTree.removeMin();
	cout << testTree.toString("message") << endl;

	testTree.removeMin();
	cout << testTree.toString("message") << endl;

	testTree.insert(17);
	cout << testTree.toString("message") << endl;

	std::string inputWord, finalWord;															
	std::cout << "please enter 2 words" << std::endl;
	std::getline(std::cin, inputWord);

	std::getline(std::cin, finalWord);

	aGame(inputWord,finalWord);
	bruteGame(inputWord, finalWord);

	return 0;
}
/* 
		brute force game
*/
void bruteGame(std::string inputWord, std::string finalWord)
{
	int sizeOfDictionary, sizeOfWord;															 

	sizeOfWord = inputWord.length();															//checking size of word
	if (inputWord.length() != finalWord.length())
	{
		std::cout << "the lengths of the words are not the same " << std::endl;
		return;
	}

	std::vector<std::string>dictionary = openfile(sizeOfWord);									//open file with only certain size of words
	sizeOfDictionary = dictionary.size();

	checkDictionaryAndMark(inputWord, sizeOfDictionary, 0, dictionary, finalWord);				//checking dictionary
	checkDictionaryAndMark(finalWord, sizeOfDictionary, 0, dictionary, finalWord);
	
	DoublyLinkedList list;
	std::vector<std::string>ladder, possibleWords;

	std::cout << "looking for a ladder, please wait... " << std::endl;

	int enqueue = 0, dequeue = 0;

	bool found = false;
	do{

		if (ladder.size() == 0)
		{
			ladder.push_back(inputWord);															//putting first word in a ladder
			possibleWords = changeLetterAndMark(inputWord, sizeOfDictionary, dictionary, finalWord);		//creating all possible words
		}

		else if (list.getSize() != 0)
		{
			std::string tempString;																	//temp string to be stored in ladder
			tempString = ladder[ladder.size() - 1];													//pulling off last word in ladder to be iterated
			possibleWords = changeLetterAndMark(tempString, sizeOfDictionary, dictionary, finalWord);
		}

		for (int i = 0; i < possibleWords.size(); i++)
		{
			std::vector<std::string>tempLadder;														//temp ladder	to be stored in queue
			if (ladder.size() > 1)
			{
				tempLadder = ladder;																//setting all previous ladders to temp ladder		
			}
			else
			{
				tempLadder.push_back(inputWord);
			}
			tempLadder.push_back(possibleWords[i]);

			if (tempLadder[tempLadder.size() - 1] == finalWord)										//looking for final word				
			{
				std::cout << "a ladder was found " << std::endl;
				std::cout << " [";
				for (int i = 0; i < tempLadder.size(); i++)
					std::cout << tempLadder[i] << ", ";
				std::cout << "] " << tempLadder.size() << std::endl;
				std::cout << "brute force was used" << std::endl << "dequeued: " << dequeue << std::endl << "engueued: " << enqueue << std::endl;

				found = true;
				break;
			}
			else
			{
				list.add(tempLadder);																//adding to queue									
				enqueue++;
			}
		}
		if (ladder.size() >= 10)
			found == true;

		ladder = list.pop_back();																//pulling from queue
		dequeue++;
	} while (found == false);

}
void aGame(std::string inputWord, std::string finalWord)                                //pulls off nothing if the letter change is one away
{
	int sizeOfDictionary, sizeOfWord;

	sizeOfWord = inputWord.length();															//checking size of word
	if (inputWord.length() != finalWord.length())
	{
		std::cout << "the lengths of the words are not the same " << std::endl;
		return;
	}

	std::vector<std::string>dictionary = openfile(sizeOfWord);									//open file with only certain size of words
	
  sizeOfDictionary = dictionary.size();

	checkDictionary(inputWord, sizeOfDictionary, 0, dictionary, finalWord);						//checking dictionary
	checkDictionary(finalWord, sizeOfDictionary, 0, dictionary, finalWord);

	AvlTree<wordLadderState> tree;
	wordLadderState ladderWithScore;

	std::vector<std::string> possibleWords;
	int enqueue = 0, dequeue = 0;

	std::cout << "looking for a ladder, please wait... " << std::endl;

  bool found = false;
  do{

    ladderWithScore.setFirstWord(inputWord);
    ladderWithScore.setLastWord(finalWord);

    if (ladderWithScore.getSize() == 0)
    {
      ladderWithScore.push_back(inputWord);															                          //putting first word in a ladder
      possibleWords = changeLetter(inputWord, sizeOfDictionary, dictionary, finalWord);		//creating all possible words
    }

    else if (tree.getSize() != 0)
    {
      std::string tempString;																	                                    //temp string to be stored in ladder
      tempString = ladderWithScore.getWord(ladderWithScore.getSize() - 1);												//pulling off last word in ladder to be iterated
      possibleWords = changeLetter(tempString, sizeOfDictionary, dictionary, finalWord);
    }

    for (int i = 0; i < possibleWords.size(); i++)
    {
      wordLadderState tempLadder;														        //temp ladder	to be stored in tree
      if (ladderWithScore.getSize() > 1)
      {
        tempLadder = ladderWithScore;																//setting all previous ladders to temp ladder		
      }
      else
      {
        tempLadder.push_back(inputWord);
      }
      tempLadder.push_back(possibleWords[i]);

      if (tempLadder.getWord(tempLadder.getSize() - 1) == finalWord)										//looking for final word				
      {
        std::cout << "a ladder was found " << std::endl;
        std::cout << tempLadder.toString() << std::endl;
		std::cout << "Astar was used" << std::endl << "dequeued: " << dequeue << std::endl << "engueued: " << enqueue << std::endl;
        found = true;
        break;
      }
      else
      {
        tree.insert(tempLadder);																//adding to tree									
		enqueue++;
      }
    }
    if (ladderWithScore.getSize() >= 10)
      found == true;

    ladderWithScore = tree.removeMin();																//pulling from tree
	dequeue++;
  } while (found == false);

}

std::vector<std::string> changeLetterAndMark(std::string input, int sizeOfDictionary, std::vector<std::string>&dictionary, std::string finalWord)
{
	std::vector<std::string>possibleWordLadder;
	for (int p = 0; p < input.length(); p++)
	{
		std::string temp = input;
		temp[p] = 'a';
		for (int i = 0; i < 25; i++)
		{
			if (checkDictionaryAndMark(temp, sizeOfDictionary, 0, dictionary, finalWord))
			{
				//std::cout << temp << std::endl;													//for debugging
				possibleWordLadder.push_back(temp);
			}
			temp[p] += 1;
		}
	}
	return possibleWordLadder;
}
std::vector<std::string> changeLetter(std::string input, int sizeOfDictionary, std::vector<std::string>&dictionary, std::string finalWord)
{
	std::vector<std::string>possibleWordLadder;
	for (int p = 0; p < input.length(); p++)
	{
		std::string temp = input;
		temp[p] = 'a';
		for (int i = 0; i < 25; i++)
		{
			if (checkDictionary(temp, sizeOfDictionary, 0, dictionary, finalWord))
			{
				//std::cout << temp << std::endl;													//for debugging
				possibleWordLadder.push_back(temp);
			}
			temp[p] += 1;
		}
	}
	return possibleWordLadder;
}

std::vector<std::string>openfile(int size)
{
	std::vector<std::string>dictionary;
	std::ifstream fin;

	fin.open("dictionary.txt");
	if (!fin.is_open())
		std::cout << "error opening file" << std::endl;

	do{
		std::string temp;
		fin >> temp;
		if (temp.length() == size)													//only allows the same size words in the dictionary
		{
			dictionary.push_back(temp);
		}
	} while (fin.good());

	/*for (std::vector<std::string>::const_iterator i = dictonary.begin(); i != dictonary.end(); ++i)				//from overstack.com
	std::cout << *i << ' ';																							//for debugging
	*/

	return dictionary;
}

bool checkDictionaryAndMark(std::string inputWord, int end, int beg, std::vector<std::string> &dictionary, std::string finalWord)					//binary search
{


	int mid = (end + beg) / 2;

	if (dictionary[mid].length() != inputWord.length())
		if (!dictionary[mid].find('$') == dictionary[mid].length() - 1)
			return false;

	if (dictionary[mid] == inputWord)
	{
		if (dictionary[mid] == finalWord)
			return true;
		else
		{
			dictionary[mid] = dictionary[mid] + "$";
			return true;
		}
	}
	else if (end - beg <= 1)
	{
		//std::cout << "it checked the dictionary and counldn't find it" << std::endl;								//for debugging
		return false;
	}
	else if (dictionary[mid] > inputWord)
		return checkDictionaryAndMark(inputWord, mid, beg, dictionary, finalWord);
	else
		return checkDictionaryAndMark(inputWord, end, mid, dictionary, finalWord);
}
bool checkDictionary(std::string inputWord, int end, int beg, std::vector<std::string> &dictionary, std::string finalWord)					//binary search
{


	int mid = (end + beg) / 2;

	if (dictionary[mid].length() != inputWord.length())
		//if (!dictionary[mid].find('$') == dictionary[mid].length() - 1)
			return false;

	if (dictionary[mid] == inputWord)
	{
		if (dictionary[mid] == finalWord)
			return true;
		else
		{
			dictionary[mid] = dictionary[mid] + "$";
			return true;
		}
	}
	else if (end - beg <= 1)
	{
		//std::cout << "it checked the dictionary and counldn't find it" << std::endl;								//for debugging
		return false;
	}
	else if (dictionary[mid] > inputWord)
		return checkDictionaryAndMark(inputWord, mid, beg, dictionary, finalWord);
	else
		return checkDictionaryAndMark(inputWord, end, mid, dictionary, finalWord);
}
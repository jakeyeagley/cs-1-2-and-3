#include <iostream>
#include <string>

using namespace std;

#ifndef SkewHeap_H
#define SkewHeap_H

struct node
{
    int key;
    node *left;
    node *right;
    node(int k = 0, node * l = NULL, node * r = NULL) : key(k), left(l), right(r) {}
};

class maxHeap
{
  int size, mergeCt;
    node * root;

public:
  maxHeap(void) { size = 0; mergeCt = 0; root = NULL; }
	node * merge(node * h1, node * h2);
  int remove();
  void insert(int newValue);
  int find() { if(root != NULL) return root->key; else return 0; }
	int getSize() { return size; }
  int getMerge() { return mergeCt; }
	string toString();
};

class minHeap
{
    int size, mergeCt;
    node * root;

public:
  minHeap(void) { size = 0; mergeCt = 0; root = NULL; }
	node * merge(node * h1, node * h2);
  int remove();
  void insert(int newValue);
  int find() { if (root != NULL) return root->key; else return 0; }
	int getSize() { return size; }
  int getMerge() { return mergeCt; }
	string toString();
};
#endif
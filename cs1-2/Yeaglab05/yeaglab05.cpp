#include <iostream>
#include "yeagcomplex.h"
//This program takes an 2 imaginary numbers in the form a + bi and adds or subtracts them together
using namespace std;
using namespace Yeagley;

int main()
{
	Complex c1;						//data types for both complex numbers and the total
	Complex c2;
	Complex c3;
	double x;						//for the input

									//inputting first real
	cout << "This program takes an 2 imaginary numbers in the form a + bi and adds and subtracts them together" << endl;
	cout << "please enter a number for the real part" << endl;
									
	cin >> x;
	c1.setReal(x);
									//imaginary 
	cout << "please enter a number for the imaginary part" << endl;
	cin >> x;
	c1.setImaginary(x);
									//inputting second real
	cout << "now please enter a second number for the second real part" << endl;
	cin >> x;
	c2.setReal(x);
									//imaginary
	cout << "please enter a second number for the second imaginary part" << endl;
	cin >> x;
	c2.setImaginary(x);

									//adding and subtracting functions
	c3 = c1.addComplex(c2);
	c3.print();
	
	c3 = c1.subComplex(c2);
	c3.print();

	return 0;

}
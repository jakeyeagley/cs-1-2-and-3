#include "DoublyLinkedList.h"

void DoublyLinkedList::add(std::vector<std::string> ladder)
{
	Node * newNode = new Node(ladder);
	if (head == NULL)
	{
		head = newNode;
		tail = newNode;
		size++;
	}

	else
	{
		tail->next = newNode;
		newNode->prev = tail;
		tail = newNode;
		size++;
	}
}

std::vector<std::string> DoublyLinkedList::get(int count)
{
	std::vector<std::string> error;
	error.push_back("error");
	int i = 0;
	for (Node *temp = head; i < count && temp != NULL; i++)
	{
		if (i==(count - 1))
			return temp->value;
		temp = temp->next;
	}
	
	return error;
}

std::vector<std::string> DoublyLinkedList::pop_back()
{
	std::vector<std::string> ladder;
	Node *temp;

	if (head == NULL)
	{
		return ladder;
		size == 0;
	}
	else
	{
		temp = head;
		head = head->next;
		size--;
		ladder = temp->value;
		return ladder;
	}

	delete temp;

}

//Jake Yeagley, Lab 10, CS 1415
#ifndef RECTANGE_H									//including safe gaurds
#define RECTANGE_H

#include <iostream>
#include "yeagShape.h"								//including shape file

using namespace std;

namespace Yeagley									//adding to the namespace
{
	class rectangle: public shape					//inheriting from the shape class
	{
	private:
		double length_;								//2 data members for 2 sides of rectangle
		double width_;

	public:											//constructor
		rectangle(double l = 0, double w = 0);
		void setLength(double);						//set functions
		void setWidth(double);
		double getLength()const;					//get functions
		double getArea()const;
		double getPerimeter()const;
		void print()const;							//print function
	};
}
#endif
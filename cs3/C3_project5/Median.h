#include "SkewHeap.h"

#ifndef Median_H
#define Median_H

using namespace std;

class median
{
    int currMedian = 0;
    int currValue = 0;

public:
    maxHeap max;
    minHeap min;
    void addNum(int n);
    void report(bool b);
    void setMedian();
};
#endif
#include "SkewHeap.h"
#include <time.h>
#include "Median.h"

using namespace std;

int main()
{
	const int TEST_VALUE = 15;
	srand(unsigned (time(0)));
    median m;

    m.addNum(5);
    m.report(true);
    m.addNum(7);
    m.report(true);
    m.addNum(10);
    m.report(true);
    m.addNum(15);
    m.report(true);
    m.addNum(12);
    m.report(true);
    m.addNum(8);
    m.report(true);
    m.addNum(3);
    m.report(true);
    m.addNum(1);
    m.report(true);
    m.addNum(2);
    m.report(true);
    m.addNum(42);
    m.report(true);
    m.addNum(20);
    m.report(true);
   
	for (int i = 0; i < TEST_VALUE; i++)
	{
		int num = rand() % 1000;
		m.addNum(num);
	}
	m.report(true);  // print Heaps
	char c;
	int size = 32;
	for (int i = TEST_VALUE; i < 1028; i++)
	{  int num = rand()%1000;
	   //cout << num << " ";
	   m.addNum(num);
	   if (i == size){
		   m.report(false);
		   size*=2;
	   }
	}
	cin >> c;
	return 0;

}
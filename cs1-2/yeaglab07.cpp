//jake yeagley
//CS 1415
//lab 07

#include <iostream>
#include <fstream>
#include <string>
//this program deals with a database file of student records
using namespace std;
const int SIZEOFFIRST = 20;														//constants
const int SIZEOFLAST = 15;

struct record																	//structure for student records
{
	char firstName[SIZEOFFIRST];												//constant for arrays
	char lastName[SIZEOFLAST];
	int age;
	double gpa;
};

void DisplaySummaryListOfEntries(fstream&);										//functions prototypes
void DisplayAnEntry(fstream&);
void AddAnEntry(fstream&);
void EditAnEntry(fstream&);

int main()
{

	int menu;																	//variable for menu
	string theFile;																//string for the file name
	fstream file;																//for inputing/outputting to file
																				//prompting
	cout << "this program deals with a database file of student records " << endl;
	cout << "what file would you like to open? " << endl;						
	getline(cin,theFile);
	
	file.open(theFile, ios::in | ios::binary | ios::out);						//open file
	
	do																			//looping for menu
	{
	
	if(!file.good())															//checking to see if it opened
		{
			file.clear();														//creating it if it's not already excisting
			file.open(theFile, ios::out);
			file.close();
			file.open(theFile, ios::in | ios::binary | ios::out);
		}

		cout << "1. display summary list of entries " << endl;
		cout << "2. display an entry " << endl;
		cout << "3. add an entry " << endl;
		cout << "4. edit an entry " << endl;
		cout << "5. quit " << endl;
		cin >> menu;															//recieving which function to call
		if(menu == 1)															//function calls
			DisplaySummaryListOfEntries(file);
		if(menu == 2)
			DisplayAnEntry(file);
		if(menu == 3)
			AddAnEntry(file);
		if(menu == 4)
			EditAnEntry(file);
		if(menu == 5)
			break;
	}while(menu != 1 || menu != 2 || menu != 3 || menu != 4 || menu != 5);		

	file.close();																//closing the file

	return 0;

}
void DisplaySummaryListOfEntries(fstream& file)									//display entries function
{
	record student;						

	file.seekg(0L,ios::beg);
	file.read(reinterpret_cast<char *>(&student),sizeof(student));
	while(!file.eof())															//loop to display last names
	{
		cout << student.lastName << endl;
		file.read(reinterpret_cast<char *>(&student),sizeof(student));
	}

	file.clear();																//clear the bit
}
void DisplayAnEntry(fstream& file)												//display a certain entry
{
	long size;
	record student;

	cout << "which entry would you like to view? " << endl;
	cin >> size;

	size = size - 1;															//resizing	
	size = size * sizeof(student);

	file.seekg(size,ios::beg);
	file.read(reinterpret_cast<char *>(&student), sizeof(student));
	cout << student.firstName << endl << student.lastName << endl 
		<< student.age << endl << student.gpa << endl;
}
void AddAnEntry(fstream& file)													//adding an entry
{
	record student;

	cout << "what is the students first name? " << endl;						//getting information
	cin >> student.firstName;
	cout << "what is the students last name? " << endl;
	cin >> student.lastName;
	cout << "what is the students age? " << endl;
	cin >> student.age;
	cout << "what is the students gpa? " << endl;
	cin >> student.gpa;

	file.write(reinterpret_cast<char *>(&student), sizeof(student));			//writing
}

void EditAnEntry(fstream& file)
{
	if(!file.good())
		cout << "bad" << endl;
	record student;
	long size;

	cout << "which record would you like to edit? " << endl;
	cin >> size;
	
	size = size - 1;															//resizing		
	size = size * sizeof(student);

	file.seekp(size,ios::beg);													//placing marker for reading
	file.read(reinterpret_cast<char *>(&student),sizeof(student));				//reading information
	
	cout << "what is the students first name? " << endl;
	cin >> student.firstName;
	cout << "what is the students last name? " << endl;
	cin >> student.lastName;
	cout << "what is the students age? " << endl;
	cin >> student.age;
	cout << "what is the students gpa? " << endl;
	cin >> student.gpa;
	
	file.seekp(size,ios::beg);													//placing marker to write
	file.write(reinterpret_cast<char *>(&student),sizeof(student));				//writing the structure
}


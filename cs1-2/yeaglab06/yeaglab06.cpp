#include <iostream>
#include "yeagstring.h"
//this program is going to show how a string class is created 
using namespace std;
using namespace Yeagley;				//using namespace

int main()
{
	String s, forconstructor("Jake");	//class data type
	char line[100];						//getting input data type
	cout << "this program is going to show how a string class is created " << endl;
										//input
	cout << "please input a line for the string" << endl;
	cin.getline(line,100);				
	s.setString(line);					//making it a string
	s.print();							//outputting string
	cout << endl;				
	cout << s.size() << endl;			//outputting size

	forconstructor.print();				//to show Garth my constructor works
	cout << endl << forconstructor.size() << endl;

	return 0;
}
#include <iostream>

#ifndef tree_H
#define tree_H
class Node
{
public:
	Node(std::string w, int kid, Node * aleft = NULL, Node *aright = NULL) : word(w), kidCount(kid), left(aleft), right(aright){}
	std::string word;
	int kidCount;

	Node *left;
	Node *right;
  Node *parent;
  Node *sibling;

	int level, subTree;
};

class Tree {
	Node * root;
public:
	Tree(Node * root = NULL) : root(root) {}
  Node* buildTreeFromPreorder(std::ifstream & file) { return buildTreeFromPreorder(file, NULL); };
	Node* buildTreeFromPreorder(std::ifstream &file, Node * parent);
	Node* findWord(std::string word, Node* n);
	Node* findWord(std::string word) { return findWord(word, root); }
	std::string printTree(std::string indent, Node * n);
	std::string printTree(std::string indent = ""){ return printTree(indent, root); }
	void makeEmpty(Node *& r);
	void makeEmpty() { makeEmpty(root); } 
	void upCase(Node* n);
	void upCase(){ upCase(root); }
	Tree clone() { return Tree(clone(root, NULL)); }
	Node * clone(Node * n, Node * prevNode);
	int numberOfSubtrees(Node * n);
	int nodesInLevel(int level){ return nodesInLevel(level, root); };
	int nodesInLevel(int level, Node * r);
	int fringe() { return fringe(root); }
	int fringe(Node * n);
	bool isIsomorphic(Tree &t2) { return isIsomorphic(root, t2.root); } 
	bool isIsomorphic(Node *r1, Node *r2);
	std::string commonAncestor(std::string s1, std::string s2); 
	Node * commonAncestor(Node* n1, Node *n2);
};

#endif
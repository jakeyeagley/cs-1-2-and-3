//Jake Yeagley, Lab 10, CS 1415
#include <iostream>
#include "yeagTriangle.h"				//including the triangle class
#include <cmath>

using namespace std;

namespace Yeagley						//adding to the namespace
{
										//constructor
	triangle::triangle(double a,double b,double h)
	{
		sideA_ = a;
		sideB_ = b;
		sideH_ = h;
	}
										//set functions
	void triangle::setSides(double sideA, double sideB, double sideH)
	{
		if(sideA < 0.0)					//checking for negatives
			sideA_ = 0.0;
		else
			sideA_ = sideA;				//setting a
		
		if(sideB < 0.0)
			sideB_ = 0.0;				//checking for negatives
		else
			sideB_ = sideB;				//setting b
		
		if(sideH < 0.0)					//checking for negative
			sideH_ = 0.0;
										//side A and B added can't be less than the hypotenuse 
		else if((sideA + sideB) < sideH)
			sideH_ = sideA + sideB;
		else
			sideH_ = sideH;				//setting h
	}
	double triangle::getArea()const		//get functions
	{
		double s, area, math;
		s = (sideA_ + sideB_ + sideH_) / 2;
		math = (s * (s-sideA_) * (s-sideB_) * (s-sideH_));
		area = sqrt(math);
		return area;
	}
	double triangle::getPerimeter()const
	{
		return sideA_ + sideB_ + sideH_;
	}
	void triangle::print()const			//print function
	{
		cout << "triangle with a side lengths of " << sideA_ << " and " << sideB_ << " and a hypotenuse of " 
			<< sideH_ << endl;
	}
}
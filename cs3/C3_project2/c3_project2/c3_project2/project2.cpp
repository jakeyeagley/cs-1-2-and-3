//#include "Game.h"
#include <iostream>
#include <fstream>
#include "tree.h"
#include <string>

int main() {
	std::ifstream fin;
	std::ofstream fout;
	fin.open("prog2In.txt");
	if (fin.is_open())
		std::cout << "file was opened" << std::endl;
	else
		std::cout << "file was not opened " << std::endl;
	
	const int SIZE = 12;
	Tree testTree[SIZE];
	for (int i = 0; i < SIZE;i++)
		testTree[i].buildTreeFromPreorder(fin);
	for (int i = 0; i < SIZE; i++){
		std::cout << "Tree " << i << "\n" << testTree[i].printTree() << "\n";
		fout << "Tree " << i << "\n" << testTree[i].printTree() << "\n";
		std::cout << "FRINGE " << testTree[i].fringe() << std::endl;
	}
	if (testTree[0].findWord("dins") == NULL) std::cout << "dins not found\n";
	if (testTree[0].findWord("tone") == NULL) std::cout << "tone not found\n";
	testTree[0].upCase();
	std::cout << testTree[0].printTree();
	if (testTree[0].findWord("guck") == NULL) std::cout << "guck not found\n";
	if (testTree[0].findWord("TONE") == NULL) std::cout << "TONE not found\n";

	testTree[7].makeEmpty();
	std::cout << "empty tree fringe " << testTree[7].fringe() << std::endl;
	for (int i = 0; i < SIZE; i++)
		std::cout << "NodesInLevel 2 of tree" << i << " " << testTree[i].nodesInLevel(2) << std::endl;
	std::cout << "Tree 3 \n" << testTree[3].printTree() << std::endl;
	std::cout << "Tree 10 \n" << testTree[10].printTree()<< std::endl;
	testTree[3] = testTree[10].clone();
	testTree[3].upCase();
	std::cout << "Tree 3 cloned\n" << testTree[3].printTree() << std::endl;
	std::cout << "Tree 10\n" << testTree[10].printTree() << std::endl;

	for (int i = 0; i < SIZE; i++)
		for (int j = i + 1; j < SIZE; j++)
			if (testTree[i].isIsomorphic(testTree[j])) std::cout << "two trees are isomorphic Tree: " << i << " Tree: " << j << std::endl;


	char c;
	std::cin >> c;
	fout.close();
	
	return 0;
	
}
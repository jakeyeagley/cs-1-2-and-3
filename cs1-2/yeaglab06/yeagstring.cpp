#include <iostream>
#include "yeagstring.h"					//using header file

using namespace std;

namespace Yeagley						//adding on to namespace
{
	String::String()					//constructor
	{
		ptr = NULL;
	}

	String::String(char s[])			//explicit constructor
	{
		size_ = strlen(s);
		ptr = new char[size_+1];		//dynamacally allocates memory
		strcpy(ptr,s);
	}

	int String::size()					//size function
	{
		return size_;
	}

	void String::setString(char s[])	//set function
	{
		size_ = strlen(s);
		ptr = new char[size_+1];		//dynamacally allocates memory
		strcpy(ptr,s);
	}

	void String::print()				//print function
	{
		cout << ptr;
	}
	
	String::~String()					//deconstrucor for memory
	{
		delete[]ptr;
	}
}
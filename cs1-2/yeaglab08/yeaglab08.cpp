#include <iostream>
#include "yeagRational.h"																		//including the Rational class
//this program does different comparasions on rationals
using namespace std;

int main()
{
	Rational R1;
	Rational R2;
	Rational R3(0,1);																			//rational data types and calling constructors
	Rational R4;
	double ans;

	cout << "this program does different comparasions on rationals" << endl;					
	cout << "please enter a rational number" << endl;
	cin >> R1;																					//input with overloaded >> and <<
	cout << "please enter a second rational number" << endl;
	cin >> R2;

	cout << "your rationals reduced are " << R1 << " and " << R2 << endl;						

	R3 = R1 + R2;																				//using overloaded +
	cout << "the two rationals added is " << R3 << endl;
	R3 = R1 - R2;																				//using overloaded -
	cout << "the two rationals subtracted is " << R3 << endl;
	R3 = R1 / R2;																				//using overloaded /
	cout << "the first rational divided by the second is " << R3 << endl;
	R3 = R1 * R2;																				//using overloaded *
	cout << "the two rationals multiplied is " << R3 << endl;
	
	if(R1 < R2)																				
		cout << "the first rational is less than the second " << endl;							//using overloaded <
	if(R1 > R2)
		cout << "the first rational is greater than the second " << endl;						//using overloaded >
	if(R1 <= R2)
		cout << "the first rational is less than or equal to the second " << endl;				//using overloaded >=
	if(R1 >= R2)
		cout << "the first rational is greater than or equal to the second " << endl;			//using overloaded <=
	if(R1 == R2)
		cout << "the two rationals are equal to each other" << endl;							// using overloaded ==
	if(R1 != R2)
		cout << "the two rationals are not equal to each other" << endl;						// using overloaded !=
	
	cout << "please enter a rational to be converted into a double" << endl;
	cin >> R4;
	ans = R4;
	cout << ans << endl;
	
	return 0;
}

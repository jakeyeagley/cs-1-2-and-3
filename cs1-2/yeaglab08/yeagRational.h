#ifndef RATIONAL_H						//including the guards
#define RATIONAL_H

#include <iostream>

using namespace std;

class Rational							//class named Rational
{
private:								//private data members of the class
	int numerator_;
	int denominator_;
	void reduced();

public:						
	Rational();							//constructors
	Rational(int, int);
	void setNumerator(int);				//public functions
	void setDenominator(int);
	void printReduced();				
	int getNumerator()const;		
	int getDenominator()const;
	Rational operator+(const Rational&);			//overloaded functions
	Rational operator-(const Rational&);
	Rational operator/(const Rational&);
	Rational operator*(const Rational&);
	bool operator<(const Rational&);
	bool operator>(const Rational&);
	bool operator<=(const Rational&);
	bool operator>=(const Rational&);
	bool operator==(const Rational&);
	bool operator!=(const Rational&);
	operator double()const;

	friend ostream &operator << (ostream&, const Rational&);			//friends of the class and overloaded functions
	friend istream &operator >> (istream&, Rational&); 

};

#endif
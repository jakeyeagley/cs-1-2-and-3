//Jake Yeagley, Lab 10, CS 1415
#ifndef SQUARE_H								//includin safe gaurds
#define SQUARE_H

#include <iostream>
#include "yeagRectangle.h"						//including the rectangle class
															
using namespace std;

namespace Yeagley								//adding to the namespace
{
	class square: public rectangle				//inheriting from the rectangle class which is inheriting from the shape class
	{
	private:									//using the data members from the rectangle
	public:
		square(double);							//constructor
		void setSide(double);					//set function
		double getArea()const;					//get functions
		double getPerimeter()const;
		void print()const;						//print function
	};
}

#endif
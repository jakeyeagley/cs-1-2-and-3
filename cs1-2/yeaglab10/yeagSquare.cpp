//Jake Yeagley, Lab 10, CS 1415
#include <iostream>
#include "yeagSquare.h"									//including both the square and rectangle .h's	
#include "yeagRectangle.h"

using namespace std;

namespace Yeagley										//adding to the namespace
{
	square::square(double side)							//constructor, using the rectangles constuctor
		:rectangle(side,side)
	{}
	void square::setSide(double s)						//set function
	{
		setLength(s);
	}
	double square::getArea()const						//get functions
	{
		return getLength() * getLength();
	}
	double square::getPerimeter()const
	{
		return 4 * getLength();
	}
	void square::print()const							//print function
	{
		cout << "square with a length of " << getLength() << endl;
	}
}
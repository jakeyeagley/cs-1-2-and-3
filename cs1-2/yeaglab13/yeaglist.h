#ifndef YEAGLIST_h							//safe guards
#define YEAGLIST_H

#include <iostream>							//including the librarys
#include <exception>
#include <new>
using namespace std;	

namespace Yeagley							//creating a namespace
{
	class nodeException: public exception	//exception class
	{
	public:
		nodeException(char message[])		//derived from the exception class
			:exception()
		{}
	};

	template<typename t>
	class list								//template class 
	{
	private:
		struct Node							//structure for nodes
		{
			int item;						//holds item
			Node *next;						//points to next node
		};
		Node *head;							//starting node

	public:
		list()								//constructor/destructor
			{head = 0;}
		~list();
		void append(t newItem);				//functions
		void remove(t item);
		void display()const;
	};

	template<typename t>
	void list<t>::append(t newItem)			//append
	{
		Node *newPtr;						//create pointers to two nodes
		Node *cur = head;
		newPtr = new Node;					//create memory for it
		newPtr -> item = newItem;			//point to the value passes
		newPtr -> next = 0;					//set equal to null
		if(head == 0)
			head = newPtr;					//head points to the new pointer if it's nowhere
		else
		{
			while(cur -> next != 0)			//look for end
			{
				cur = cur -> next;
			}
			cur -> next = newPtr;			
		}
	}

	template<typename t>
	list<t>::~list()						//destructor
	{
		Node *ptr;							//new node
		while(head != 0)					//travel down the list
		{
			ptr = head;						
			head = head -> next;
			delete ptr;						//deleting along the way
		}
	}

	template<typename t>
	void list<t>::display()const			//display function
	{
		Node *cur = head;					//create new node pointer at head
		while(cur != 0)						//travel down the list
		{
			cout << cur -> item << endl;	//output nodes
			cur = cur -> next;
		}
	}

	template<typename t>
	void list<t>::remove(t item)			//remove function
	{
		Node *cur = head;					//previous and current nodes
		Node *prev = 0;
											//travel down list
		while(cur != 0 && cur -> item != item)
		{
			prev = cur;
			cur = cur -> next;
		}
		if(cur == 0)						//error if numbers not there
			throw nodeException("error");
		else if(cur == head)				//special case 
		{
			head = head -> next;
			delete cur;
		}
		else
		{
			prev -> next = cur -> next;
			delete cur;
		}
	}
}

#endif YEAGLIST_H

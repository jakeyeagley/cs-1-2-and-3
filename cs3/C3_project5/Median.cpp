#include "Median.h"
#include <string>

using namespace std;

void median::addNum(int num)
{
    currValue = num;
    if (currMedian == 0)
        currMedian = currValue;
    else if (currValue > currMedian)
        min.insert(currValue);
    else if (currValue < currMedian)
        max.insert(currValue);

    setMedian();
}

void median::setMedian()
{
    if (max.getSize() >= min.getSize() + 2)
    {
        min.insert(currMedian);
        currMedian = max.remove();
    }
    if (min.getSize() >= max.getSize() + 2)
    {
        max.insert(currMedian);
        currMedian = min.remove();
    }

}
void median::report(bool b)
{
	if (b == true)
	{
    cout << "the min heap is " << endl << min.toString() << endl << "the min heaps size is: " << min.getSize() << endl << "the number of merges was: " << min.getMerge() << endl;
		cout << "the current median is " << currMedian << endl;
    cout << "the max heap is " << endl << max.toString() << endl << "the max heaps size is: " << max.getSize() << endl << "the number of merges was: " << max.getMerge() << endl;
	}
}

//Jake Yeagley, Lab 09, CS 1415
#ifndef SQUARE_H						//including the safe guards
#define SQUARE_H

#include <iostream>
#include "yeagPoint.h"					//including the point 

using namespace std;

namespace Yeagley						//adding to the namespace
{
	class square						//creating square class
	{
	private:							//private members
		double sideLength_;				//for area
		point lowerLeft_;				//a square 'has a' point

	public:
										//constructor with default values
		square(double s = 0.0, point lower = (0,0));		
		void setLowerLeft(point);		//set functions
		void setSideLength(double);
		double getArea()const;			//get functions
		double getSideLength()const;
										//output overloading
		friend ostream &operator << (ostream&, const square&);
	};
}
#endif
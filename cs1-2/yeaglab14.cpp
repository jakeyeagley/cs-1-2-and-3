//jake Yeagley
//CS 1415
//this program simulates a bank line
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <queue>
using namespace std;

													//random function
int random();										//simulate function
int simulate();										//function for first customer
void customer(queue<int>);
									
int main()
{													//Seeding
	srand(time(0));									//Create a queue object 
	queue<int> line;								//prompting
	cout << "This program will simulate a line at a bank. It will keep " << endl;
	customer(line);									//function call for handling the customers
	
	return 0;
}
													//return a random number between 1 and 4
int random()
{
	int random;
	random = 1 + rand() % 4;						//add 1 

	return random;
}
													//function to determine simulate time
int simulate()
{
	int time;
	cout << "Enter the number of minutes you would like the program to simulate " << endl;
	cin >> time;
													//Validation
	while(time < 0)
	{
		cout << "error, please enter a valid time" <<endl;
		cin >> time;
	}

	return time;
}
													//function for handling the customers
void customer(queue<int> line)
{	
	int arrivalT, leaveT, totalT, nextArrivalT, currentT, maxCust = 0, maxWait = 0;

													//handling first customer
	totalT = simulate();
	arrivalT = random();
													//Output
	cout << "The first customer has arrived at " << arrivalT << endl;
													//determine leave time
	leaveT = random()  + arrivalT;
													//Determinenext customers arrival time.
	nextArrivalT = arrivalT + random();
													//loop to simulate multiple customers
	for(currentT = arrivalT; currentT <= totalT; currentT++)
	{												
		if(currentT == leaveT)						//checking to see if customer should leave
		{
			cout << "The customer has left at " << currentT << " minutes" << endl;
		}		
		if(currentT == nextArrivalT)				//checking to see when next customer arrives
		{
			cout << "The next customer has arrived at " << currentT << " minutes " << endl;
			line.push(nextArrivalT);
													//Determine the next customers arrival time
			nextArrivalT = nextArrivalT + random();
		}
		if(currentT >= leaveT)						//handling if there is no customers
		{
			if(line.size() != 0)
			{
				line.pop();							//leave line
				leaveT = random() + currentT;		//update leave time
			}
		}											
		if(line.size() >= maxCust)					//Find the max number of customers in the queue at any time
			maxCust = line.size();

		if(maxWait <= leaveT - nextArrivalT)		//Find the longest time any customer had to wait in line
			maxWait = leaveT - nextArrivalT; 
	}
	
	for(; currentT <= leaveT; currentT++)			//handle customers that are in the queue after closing time
	{
		if(currentT == leaveT)						//if it is time to leave
		{
			cout << "The customer left at " << currentT << " minutes" << endl;
		}

		if(currentT >= leaveT)
		{
			if(line.size() != 0)
			{
				line.pop();							//leave line
				leaveT = random() + currentT;		//update leave time
			}
		}
		if(line.size() >= maxCust)					//Find the max number of customers in the queue at any time
			maxCust = line.size();

		if(maxWait <= leaveT - nextArrivalT)		//Find the longest time any customer had to wait in line
			maxWait = leaveT - nextArrivalT;
	}
													//Display the max number of customers
	cout << "The max number of customers in the queue at any time is: " << maxCust;
	cout << endl;
													//Display the longest wait any customer
	cout << "The longest a customer had to wait is: " << maxWait << " minutes" << endl;

}


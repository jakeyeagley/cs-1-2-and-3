#include <iostream>					
#include "yeagRational.h"										//including the class with the file

#include <cstdlib>

using namespace std;

Rational::Rational()											//constructor
{
	numerator_ = 1;
	denominator_ = 1;
}

Rational::Rational(int a, int b)								//constructor with explesit values
{
	numerator_ = a;
	denominator_ = b;
}

void Rational::setNumerator(int numerator)						//getting the numerator 
{
	if (numerator == 0)
	{
		cout << "Your fraction is 0" << endl;
		exit(EXIT_FAILURE);
	}
	else 
		numerator_ = numerator;
}

void Rational::setDenominator(int denominator)					//getting the denominator
{
	if (denominator == 0)
	{
		cout << "Your fraction is undefinded" << endl;
		exit(EXIT_FAILURE);
	}
	else
		denominator_ = denominator;
}

void Rational::reduced()										//reducing the fraction
{
	int limit, GCF = 1;											//GCF = greatest common factor
	
	if(numerator_ < denominator_)								//setting up the limits for GCF
		limit = numerator_;
	else
		limit = denominator_;

	for(int i = limit; i > 0;i--)								//reducing
	{
		if(numerator_ % i == 0 && denominator_ % i ==0)
		{
			GCF = i;
			break;
		}
	}

	numerator_ = numerator_ / GCF;								//dividing by the GCF
	denominator_ = denominator_ / GCF;

}

int Rational::getNumerator() const								//numerator
{
	return numerator_;
}

int Rational::getDenominator() const							//denominator
{
	return denominator_;
}

void Rational::printReduced()
{
	cout << numerator_ << "/" << denominator_ << endl;	
}


Rational Rational::operator+(const Rational &right)													//overloaded functions in same order as declared in the class
{
	Rational ans;

	ans.numerator_ = numerator_ * right.denominator_ + denominator_ * right.numerator_;				//math arithmatic
	ans.denominator_ = denominator_ * right.denominator_;
	
	ans.reduced();																					//reducing

	return ans;
}


Rational Rational::operator-(const Rational &right)
{
	Rational ans;																					//math arithmatic

	ans.numerator_ = numerator_ * right.denominator_ - denominator_ * right.numerator_;
	ans.denominator_ = denominator_ * right.denominator_;

	ans.reduced();																					//reducing

	return ans;
}

Rational Rational::operator/(const Rational &right)
{
	Rational ans;

	ans.numerator_ = numerator_ * right.denominator_;												//math arithmatic
	ans.denominator_ = denominator_ * right.numerator_;
	
	ans.reduced();																					//reducing																																		

	return ans;
}

Rational Rational::operator*(const Rational &right)
{
	Rational ans;

	ans.numerator_ = numerator_ * right.numerator_;													//math arithmatic
	ans.denominator_ = denominator_ * right.denominator_;

	ans.reduced();																					//reducing

	return ans;
}
 
bool Rational::operator<(const Rational &right)
{
	int a,b;
	a = numerator_ * right.denominator_;											//checking which is greater/less
	b = denominator_ * right.numerator_;

	if(a < b)
		return true;
	else 
		return false;
}

bool Rational::operator>(const Rational &right)
{
	int a,b;
	a = numerator_ * right.denominator_;											//checking which is greater/less
	b = denominator_ * right.numerator_;

	if(a > b)
		return true;
	else
		return false;
}

bool Rational::operator<=(const Rational &right)
{
	int a,b;
	a = numerator_ * right.denominator_;											//checking which is greater/less or equal
	b = denominator_ * right.numerator_;

	if(a <= b)
		return true;
	else
		return false;
}

bool Rational::operator>=(const Rational &right)
{
	int a,b;
	a = numerator_ * right.denominator_;											//checking which is greater/less or equal
	b = denominator_ * right.numerator_;

	if(a >= b)
		return true;
	else
		return false;
}

bool Rational::operator==(const Rational &right)
{
	int a,b;
	a = numerator_ * right.denominator_;											//math arthmatic to see if equal											
	b = denominator_ * right.numerator_;

	if(a == b)
		return true;
	if(a != b)
		return false;
}

bool Rational::operator!=(const Rational &right)
{
	int a,b;
	a = numerator_ * right.denominator_;											//math arthmatic to see if equal
	b = denominator_ * right.numerator_;

	if(a != b)
		return true;
	if(a == b)
		return false;
}

ostream &operator << (ostream &out, const Rational &obj)							//friend of the class output
{
	out << obj.numerator_;															//adding a / sign in outputting
	if(obj.denominator_ != 1)
		out << "/" << obj.denominator_;

	return out;
}

istream &operator >> (istream &in, Rational &obj)									//friend of the class input
{
	in >> obj.numerator_;											

	char ch;
	if(in.peek() == '/')															//checking for a zero 
	{
		in.get(ch);
			if(isdigit(in.peek()) && in.peek() != '0')
				in >> obj.denominator_;
			else
				obj.denominator_ = 1;
	}
	else 
		obj.denominator_ = 1;
	
	obj.reduced();

	return in;
}

Rational::operator double()const
{
	double x,y;
	x = static_cast<double>(numerator_);
	y = static_cast<double>(denominator_);

	return x / y;											
}

	
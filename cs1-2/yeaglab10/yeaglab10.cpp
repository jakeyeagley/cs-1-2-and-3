//Jake Yeagley, Lab 10, CS 1415
//this program uses and demonstrates polymorphism 
#include <iostream>										//including all the files
#include "yeagRectangle.h"
#include "yeagCircle.h"
#include "yeagShape.h"
#include "yeagSquare.h"
#include "yeagTriangle.h"

using namespace std;
using namespace Yeagley;					//using the namespace

void showTheRectangleClass(rectangle&);		//prototypine for modularizing
void showTheCircleClass(circle&);
void showTheTriangleClass(triangle&);
void showTheSquareClass(square&);

int main()
{
	rectangle r;							//creating the instances
	circle c;
	triangle t;
	square s(0.0);
											//prompting
	cout << "this program measures the area and perimeter of different shapes " << endl;

	showTheRectangleClass(r);				//calling the functions
	showTheCircleClass(c);
	showTheTriangleClass(t);
	showTheSquareClass(s);

	return 0;
}
											//rectangle function
void showTheRectangleClass(rectangle &r)	
{
	double w, l;							//creating variables to hold length and width
	shape *ptr;								//creating a shape pointer
	ptr = &r;								//that's pointing to the rectangle class
											//inputting
	cout << "please enter a length and a width for the rectangle " << endl;
	cin >> l >> w;
											//setting values
	r.setLength(l);
	r.setWidth(w);
											//outputting
	ptr->print();
	cout << "the area is " << ptr->getArea() << endl;
	cout <<	"the perimeter is " << ptr->getPerimeter() << endl;
}
											//circle function
void showTheCircleClass(circle &c)
{
	double r;								//creating a variable to hold the radius
	shape *ptr;								//creating a shape pointer
	ptr = &c;								//that's pointing to the circle class
											//inputting
	cout << "please enter a radius for the circle " << endl;
	cin >> r;

	c.setRadius(r);							//setting the radius
			
	ptr->print();							//outputting
	cout << "the area is " << ptr->getArea() << endl;
	cout << "the perimeter or circumference is " << ptr->getPerimeter() << endl;
}
											//triangle function
void showTheTriangleClass(triangle &t)
{
	double a, b, h;							//variables to store 3 sides
	shape *ptr;								//creating a shape pointer
	ptr = &t;								//that's pointing at the triangle class
											//inputting
	cout << "please enter two sides and a hypotenuse for the triangle " << endl;
	cin >> a >> b >> h;

	t.setSides(a,b,h);						//setting the sides

	ptr->print();							//outputting
	cout << "the area is " << t.getArea() << endl;
	cout << "the perimeter is " << t.getPerimeter() << endl;
}
											//square function
void showTheSquareClass(square &s)
{
	double side;							//variable to store the side
	shape *rptr;							//creating a shape pointer
	rptr = &s;								//that's pointing at a square class
											//inputting
	cout << "please enter a side for the square " << endl;
	cin >> side;

	s.setLength(side);						//setting the side

	rptr->print();							//outputting
	cout << "the area is " << rptr->getArea() << endl;
	cout << "the perimeter is " << rptr->getPerimeter() << endl;
}
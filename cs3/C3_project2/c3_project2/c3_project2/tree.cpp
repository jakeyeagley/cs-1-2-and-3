#include "tree.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>


Node* Tree::buildTreeFromPreorder(std::ifstream &file, Node * parent)
{
	std::string word;
	int kidCount;
	Node * temp;
	file >> word >> kidCount;
	
	Node *n = new Node(word,kidCount,NULL,NULL);
	if (kidCount == 0)
		return n;
  if (kidCount >= 1)
  {
    n->left = buildTreeFromPreorder(file,n);
    n->left->parent = n;
    temp = n->left;
    for (int i = 1; i < kidCount; i++){
      temp->right = buildTreeFromPreorder(file,n);
      temp->right->sibling = temp;
      temp = temp->right;
    }
  }
	root = n;
	return n;
}
int Tree::numberOfSubtrees(Node * n)                                          
{
  n->subTree = 1;
	if (n->kidCount == 0)
		return n->subTree; 
  for (Node* i = n->left; i != NULL; i = i->right)
  {
    n->subTree += numberOfSubtrees(i);
  }

  return n->subTree;
}


Node* Tree::findWord(std::string word,Node* n)                             
{
	if (n == NULL)													                    
		return NULL;
	if (n->word == word)
		return n;
	Node * leftResult = findWord(word, n->left);
	if (leftResult != NULL) return leftResult;
	Node * rightResult = findWord(word, n->right);
	if (rightResult != NULL) return rightResult;

  return NULL;
}

std::string Tree::printTree(std::string indent, Node * currNode)
{	
	if (currNode == NULL) return "";
	std::string treeString;
	treeString += indent;
	treeString += currNode->word;
	treeString += "[ ";
	numberOfSubtrees(currNode);
	treeString += std::to_string(currNode->subTree);
	treeString += ":";
	treeString += " ";
	treeString += std::to_string(currNode->kidCount);
	treeString += " ]";
	indent += " ";
  
	for (Node* kid = currNode->left; kid != NULL; kid = kid->right)
	{
		treeString += "\n";
		treeString += printTree(indent, kid);
	}

	return treeString;
}

void Tree::makeEmpty(Node *& n)
{
  if (n != NULL)                                     
  {
    makeEmpty(n->left);                                         //parts taken from overstack.com
    makeEmpty(n->right);
    if (n->left != NULL)
      n->left = NULL;
    if (n->right != NULL)
      n->right = NULL;
    n = NULL;
	}
  
  delete(n);
	return;
}

void Tree::upCase(Node* n)
{
	if (n != NULL)
	{
		n->word[0] = toupper(n->word[0]);										
		upCase(n->left);
		upCase(n->right);
	}

	return;
}

Node* Tree::clone(Node * n, Node * prevNode)
{
  if (n == NULL)
    return NULL;
  Node* newNode = new Node(n->word,n->kidCount,n->left,n->right);
  newNode->subTree = n->subTree;
  newNode->level = n->level;
  prevNode = n;

  newNode->left = clone(n->left, n);
  newNode->right = clone(n->right, n);

  return newNode;
}

int Tree::fringe(Node * n)
{
	if (n == NULL)
		return 0;
	if (n->kidCount == 0)
		return 1 + fringe(n->right);
	else
		return fringe(n->left) + fringe(n->right);
}

int Tree::nodesInLevel(int level,  Node* n)
{
	if (n == NULL)
		return 0;
	if (level > 0)
	{
		level--;
		return nodesInLevel(level, n->left) + nodesInLevel(level, n->right);
	}
	if (level == 0)
		return 1 + nodesInLevel(level, n->right);
}

bool Tree::isIsomorphic(Node *n1, Node *n2)
{
  if (n1 == NULL && n2 == NULL)
    return true;
	if (n1 != NULL && n2 != NULL)
    return isIsomorphic(n1->left, n2->left) && isIsomorphic(n1->right, n2->right);
	else
		return false;
}

/*
Node * commonAncestor(Node* n1, Node *n2)
{
	for (Node * dad = n1; dad != NULL; dad = dad->parent)
		for (Node * mom = n2; mom != NULL; mom = mom->parent)

}*/
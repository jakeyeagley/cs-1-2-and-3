//Jake Yeagley, Lab 09, CS 1415
#include <iostream>
#include "yeagCube.h"								//including the .h file

using namespace std;

namespace Yeagley									//adding to the namespace
{													//constructor
	cube::cube(double side) : square()				//calling the squares constructor too
	{
		setSideLength(side);
	}

	double cube::getArea()const						//get functions
	{
		return square::getArea()* 6;
	}

	double cube::getVolume()const
	{
		double volume;
		volume = getSideLength();
		return volume * volume * volume;
	}

}


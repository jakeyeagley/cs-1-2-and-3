#ifndef WORD_H
#define WORD_H

#include <iostream>
#include <vector>
#include <ostream>
#include <sstream>

class word
{
public:
  int timesUsed;
  std::string nameOfWord;

  word() { timesUsed = 0; nameOfWord = ""; };
  word(std::string s) { nameOfWord = s; };
  word(std::string s, int x) { nameOfWord = s, timesUsed = x; };
  void setTimesUsed(int x) { timesUsed = x; };

  
  std::string toString()
  {
    std::stringstream ss;
    ss << "word: " << nameOfWord << ", " << "times seen: " << timesUsed << std::endl;
    return ss.str();
  }
  
  void operator=(const word* arg) { nameOfWord = arg->nameOfWord; timesUsed = arg->timesUsed; };
};

#endif
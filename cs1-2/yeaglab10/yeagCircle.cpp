//Jake Yeagley, Lab 10, CS 1415
#include <iostream>
#include "yeagCircle.h"

using namespace std;

namespace Yeagley
{
	circle::circle(double r)
	{
		radius_ = r;
	}
	void circle::setRadius(double radius)
	{
		if(radius < 0.0)
			radius_ = 0;
		else
			radius_ = radius;
	}
	double circle::getArea()const
	{
		return 3.1415 * radius_ * radius_;
	}
	double circle::getPerimeter()const
	{
		return 2 * 3.1415 * radius_;
	}
	void circle::print()const
	{
		cout << "circle with a radius of " << radius_ << endl;
	}
}
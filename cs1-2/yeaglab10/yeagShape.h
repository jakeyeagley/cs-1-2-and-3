//Jake Yeagley, Lab 10, CS 1415
#ifndef SHAPE_H					//including the safe guards
#define SHAPE_H

#include <iostream>

using namespace std;

namespace Yeagley				//creating a namespace
{
	class shape
	{
	private:					//no data members, the derived classes will have those
	public:
		virtual double getArea()const = 0;			//pure virtual functions for derived classes
		virtual double getPerimeter()const = 0;
		virtual void print()const = 0;		
	};
}
#endif
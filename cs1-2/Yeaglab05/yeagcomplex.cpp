#include <iostream>
#include "yeagcomplex.h"

using namespace std;

namespace Yeagley
{

	Complex::Complex()
	{
		real_ = 0.0;
		imaginary_ = 0.0;
	}

	void Complex::setReal(double i1)
	{
		real_ = i1;
	}

	void Complex::setImaginary(double i2)
	{
		imaginary_ = i2;
	}

	double Complex::getReal() const
	{
		return real_;
	}

	double Complex::getImaginary() const
	{
		return imaginary_;
	}

	Complex Complex::addComplex(Complex add)
	{
		Complex sum;
		sum.real_ = real_ + add.real_;
		sum.imaginary_ = imaginary_ + add.imaginary_;
		return sum;
	}

	Complex Complex::subComplex(Complex sub)
	{
		Complex i;
		i.real_ = real_ - sub.real_;
		i.imaginary_ = imaginary_ - sub.imaginary_;
		return i;
	}

	void Complex::print()
	{
		cout << real_ << " + " << imaginary_ << "i" << endl;
	}
}

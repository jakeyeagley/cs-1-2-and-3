//Jake Yeagley, Lab 09, CS 1415
#ifndef POINT_H									//including the safe gaurds
#define POINT_H

#include <iostream>							
using namespace std;

namespace Yeagley								//creating a namespace			
{
	class point									//creating the point class
	{
	private:
		int x_;									//x and y cordinates
		int y_;

	public:
		point(int x = 0,int y = 0);				//constructor with default values
		int getx()const;						//get functions
		int gety()const;
		void setx(int);							//set functions
		void sety(int);
												//overloading the output 
		friend ostream &operator << (ostream&, const point&);
	};
}

#endif
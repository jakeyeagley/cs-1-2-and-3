//Jake Yeagley lab11 CS 1415 
//this program shows how to use throws and catches as error checkers
#ifndef EXCEPTION_H								//safe guards
#define EXCEPTION_H

#include <exception>							//including the exception library
using namespace std;

namespace Yeagley								//creating a namespace
{												//class for overflow error
	class overFlowException: public exception
	{
	public:
		overFlowException(char message[])		//derived from the exception class
			:exception(message)
		{}
	};
												//class for subscript error
	class subscriptException: public exception
	{
	public:
		subscriptException(char message[])
			:exception(message)					//derived from the exception class
		{}
	};
												//class for memory error
	class memoryException: public exception
	{
	public:
		memoryException(char message[])			//derived from the exception class
			:exception(message)
		{}
	};
}
#endif EXCEPTION_H

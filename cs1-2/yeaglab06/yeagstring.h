#ifndef string_H					//protectors
#define string_H

namespace Yeagley					//creating namespace
{

	class String					//creating class	
	{
	private:						//data members
		char *ptr;
		int size_;
			
	public:							//memeber funcitons
		String();
		String(char*);
		int size();
		void setString(char[]);
		void print();
		~String();
	};
}

#endif
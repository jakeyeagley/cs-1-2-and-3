//Jake Yeagley, Lab09, CS 1415
//this program deminstrates both 'has a' relationship and 'is a' relationship of classes
#include <iostream>
#include "yeagCube.h"							//including the cube, which has everything needed

using namespace std;
using namespace Yeagley;						//using the namespace

void deminstratePoint(point &p);				//prototyping functions 
void deminstrateSquare(square &s, point &p);
void deminstrateCube(cube &c, point &p);

int main()										//driver function
{
	point p;									//creating objects
	square s;
	cube c;
												//prompting
	cout << "this program deminstrates both 'has a' relationship and 'is a' relationship of classes" << endl;
	
	deminstratePoint(p);						//calling functions 
	deminstrateSquare(s,p);						//modualizing 
	deminstrateCube(c,p);

	return 0;
}

void deminstratePoint(point &p)					//deminstrating the point
{
	int holdx, holdy;							//varibales to hold x and y
												//deminstrating the constructor for the point
	cout << "the constructors values for the point are " << p << endl;
	cout << "Please input a value for the x cordinate" << endl;
	cin >> holdx;								//input
	
	p.setx(holdx);								//calling set function for x point
	
	cout << "please input a value for the y cordinate" << endl;
	cin >> holdy;								//input

	p.sety(holdy);								//calling set function for y point

	cout << "your point is " << p << endl;		//outputting
}

void deminstrateSquare(square &s, point &p)		//deminstrating the square function
{
	double holddouble;							//variable to hold the length
												//deminstrating the constructor 
	cout << "the constructors value for the square are ";
	operator<<(cout,s) << endl;
	cout << "please enter a length for a square " << endl;
	cin >> holddouble;							//inputting

	s.setSideLength(holddouble);				//calling the set side length function
	s.setLowerLeft(p);							//setting the squares point to p
												//outputting
	cout << "the area is " << s.getArea() << endl;
	operator<<(cout,s) << endl;					//showing the output is overloaded
}

void deminstrateCube(cube &c, point &p)
{
	double holddouble;							//variable to hold the cubes length
												//deminstrating the constructor 
	cout << "the constructors value for the cube are ";
	operator<<(cout,c) << endl;
	cout << "please enter a length for a cube " << endl;
	cin >> holddouble;							//inputting

	c.setSideLength(holddouble);				//setting the cubes length
	c.setLowerLeft(p);							//setting the cubes point
												//outputting
	cout << "the surface area is " << c.getArea() << endl;
	cout << " the Volume is " << c.getVolume() << endl;
	operator<<(cout,c) << endl;					//showing the output is overloaded

}

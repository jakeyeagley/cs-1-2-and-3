//Jake Yeagley, Lab 10, CS 1415
#ifndef CIRCLE_H								//including safe gaurds
#define CIRCLE_H

#include <iostream>
#include "yeagShape.h"							//including the shape class

using namespace std;

namespace Yeagley								//adding to the namespace
{
	class circle: public shape					//inheriting from the shape class
	{
	private:
		double radius_;							//hold the radius value

	public:
		circle(double r = 0);					//constructor
		void setRadius(double);					//set function
		double getArea()const;					//get functions
		double getPerimeter()const;
		void print()const;						//print function
	};
}
#endif
#include "SkewHeap.h"
#include <string>
#include <sstream>

using namespace std;

void swapKids(node * h);
string toString2(node *t, string indent);

void swapKids(node * heap)
{
	if (heap == NULL)
		return;
    else
    {
		//std::cout << "I'm swapping the kids of " << heap->key << std::endl;
        node * temp = heap->left;
        heap->left = heap->right;
		heap->right = temp;
    }
}
node * maxHeap::merge(node * h1, node * h2)
{
    node * large;
    if (h1 == NULL) return h2;
    if (h2 == NULL) return h1;
    if (h1->key > h2->key)
    {
        h1->right = merge(h1->right, h2);
        large = h1;
    }
    else
    {
        h2->right = merge(h2->right, h1);
        large = h2;
    }

    swapKids(large);
    mergeCt++;
    return large;
}
void maxHeap::insert(int newValue)
{
    node * newNode = new node(newValue);
    root = merge(newNode, root);
    size++;
}
int maxHeap::remove()                                                  
{
	if (root == NULL)
		return -1;
	
  node * temp = root;
  int value = root->key;
  
  root = merge(root->left, root->right);
	delete temp;
  size--;
  
  return value;
}
node * minHeap::merge(node * h1, node * h2)
{
    node * small;
    if (h1 == NULL) return h2;
    if (h2 == NULL) return h1;
    if (h1->key < h2->key)
    {
        h1->right = merge(h1->right, h2);
        small = h1;
    }
    else
    {
        h2->right = merge(h2->right, h1);
        small = h2;
    }

    swapKids(small);
    mergeCt++;
    return small;
}
void minHeap::insert(int newValue)
{
    node * newNode = new node;
    newNode->key = newValue;
    if (root == NULL)
        root = newNode;
    else
        root = merge(newNode, root);
    size++;
}
int minHeap::remove()
{
	if (root == NULL)
		return -1;
	node * temp = root;
	int value = root->key;

	root = merge(root->left, root->right);
	delete temp;

	size--;
	return value;
}
string minHeap::toString()
{
	return toString2(root, "");
}
string maxHeap::toString()
{
	return toString2(root, "");
}
string toString2(node *t, string indent)
{
	if (t == nullptr)
		return "";
	string treeString = to_string(t->key);
	return toString2(t->right, indent + "      ") + '\n' + indent + treeString + '\n' + toString2(t->left, indent + "      ");
}
//Jake Yeagley
//CS 1415
//lab 13
//linked list
//this program creates a linked list and can add, remove, or display data within
#include "yeaglist.h"						//including the .h file

using namespace std;						//using namespaces
using namespace Yeagley;

int main()
{
	int menu;								//variable for menu
	list<int> linkList;						//creating the list
											//prompting
	cout << "this programs store, remove, and display data in a linked list " << endl;
	cout << "1: append" << endl;
	cout << "2: display" << endl;
	cout << "3: remove" << endl;

	cin >> menu;							//input for menu
	try										//try block
	{
		do									//loop for menu
		{
			if(menu == 1)					//1 is append
			{
				int appendNumber;			//local variable to add appended number
											//prompting
				cout << endl << "what number would you like to append? " << endl;
				cin >> appendNumber;		//input
				cout << endl;
											//function call
				linkList.append(appendNumber);
											//telling the user what happened
				cout << appendNumber << " has been added to the end" << endl;
			}
			else if(menu == 2)				//2 is diplay
			{								
				cout << endl << "you list is " << endl;
				linkList.display();			//function call
				cout << endl;
			}
			else if(menu == 3)				//3 is remove
			{
				int item;					//local variable to know what to remove
											//prompting
				cout << "what item would you like to delete? " << endl;
				cin >> item;				//input
				linkList.remove(item);		//function call
				cout << item << " has been remove" << endl;
			}
			else							//exception handling
				throw nodeException("error: value not known");
											//repeating menu
			cout << "1: append" << endl;
			cout << "2: display" << endl;
			cout << "3: remove" << endl;
			cout << "4: to quit" << endl;
			cin >> menu;
		}while(menu != 4);
	}
	catch(nodeException e)					//catch block
	{
		cout << e.what() << endl;			//output error
	}

	return 0;
}
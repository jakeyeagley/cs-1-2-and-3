#include <iostream> 
#include "HashTable.h"
#include <fstream>
#include "word.h"

void printTable(HashTable<std::string,word>&,int);
int sumOfLetters(std::string input);
int lengthValue(std::string input);
int bonus(int timesUsed);
int setScore(word*);
int setScore(word);

int main(){
  HashTable<std::string,word> table;
  int ct = 0;
  HashTable<int, int>table2;

  std::ifstream fin;
  fin.open("game0.txt");                                                                //put the file you want to test here
  if (!fin.is_open())
    std::cout << "it didn't open" << std::endl;
  
  std::string tempWord;
  while (fin >> tempWord)
  {
    word *entryPtr = table.find(tempWord);
    
    if (entryPtr == NULL)
    {
      table.insert(tempWord, new word(tempWord, 1));
      word entry(tempWord, 0);
      std::cout << "word: " << tempWord << " : score: " << setScore(entry) << std::endl;
      ct++;
    }
    else
    {
        std::cout << tempWord << ", already there " << std::endl;
        entryPtr->timesUsed++;
        std::cout << "word: " << tempWord << " : score: " << setScore(entryPtr) << std::endl;
    }
    
  }
  printTable(table,ct);

  return 0;

}
int setScore(word* w)
{
  return sumOfLetters(w->nameOfWord) * lengthValue(w->nameOfWord) * bonus(w->timesUsed);
}
int setScore(word w)
{
  return sumOfLetters(w.nameOfWord) * lengthValue(w.nameOfWord) * bonus(w.timesUsed);
}
int sumOfLetters(std::string input)
{
  if (input.size() == 1 || input.size() == 2)
    return 0;
  if (input.size() == 3)
    return 1;
  if (input.size() == 4)
    return 2;
  if (input.size() == 5)
    return 3;
  if (input.size() == 6)
    return 4;
  if (input.size() == 7)
    return 5;
  if (input.size() >= 8)
    return 6;
}
int lengthValue(std::string input)
{
  int sum = 0;
  int points[26] {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};

  for (int i = 0; i < input.length(); i++)
  {
    sum += points[input[i] - 'a'];
  }

  return sum;
}
int bonus(int timesUsed)
{
  if (timesUsed == 0)
    return 5;
  if (timesUsed == 1 || timesUsed == 2 || timesUsed == 3)
    return 4;
  if (timesUsed == 4 || timesUsed == 5 || timesUsed == 6)
    return 3;
  if (timesUsed == 7 || timesUsed == 8 || timesUsed == 9)
    return 2;
  if (timesUsed >= 10)
    return 1;
}
void printTable(HashTable<std::string, word> &table, int ct)
{
    std::cout << table.toString(977);
    std::cout << "the find count was " << table.getFindCount() << std::endl;
    std::cout << "the Prob count was " << table.getProbCount() << std::endl;
}

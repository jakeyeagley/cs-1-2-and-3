//Jake Yeagley, Lab 09, CS 1415
#include <iostream> 
#include "yeagSquare.h"										//including the .h file

using namespace std;

namespace Yeagley											//adding to the namespace
{
	square::square(double s, point lower)					//constructor for the square
	{														
		lowerLeft_ = lower;									
		sideLength_ = s;
	}

	void square::setLowerLeft(point lowerLeft)				//set functions
	{
		lowerLeft_ = lowerLeft;
	}

	void square::setSideLength(double sideLength)
	{
		if(!(sideLength < 0))								//shouldn't have a negative number
			sideLength_ = sideLength;						//sets it to zero if it is negative
		else 
			sideLength_ = 0;
	}

	double square::getArea()const							//get functions
	{
		return sideLength_ * sideLength_;
	}
	
	double square::getSideLength()const
	{
		return sideLength_;
	}
															//overloading the out put operator
	ostream &operator << (ostream& out,  const square &obj)
	{
		out << obj.sideLength_ << " " << obj.lowerLeft_;
		return out;
	}
}
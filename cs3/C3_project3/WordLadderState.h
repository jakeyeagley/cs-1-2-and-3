#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

#ifndef WordLadderState_H
#define WordLadderState_H

class wordLadderState
{
	std::vector<std::string>ladder;
	std::string firstWord, lastWord;
	int score;									//score = size of ladder + letters left to change
	int size;
	int lettersLeft;
public:
  wordLadderState() { firstWord = ""; lastWord = ""; size = 0; lettersLeft = 0; score = 0; };
  int getScore(){ return score; };
  int getSize(){ return size; };
	void setScore();
  std::string getWord(int i) { return ladder[i]; }
	void push_back(std::string word);
  void setFirstWord(std::string word) { firstWord = word; };
  void setLastWord(std::string word) { lastWord = word; };
  std::string toString();
  bool operator > (wordLadderState & op2)const { return score > op2.score; };
  bool operator < (wordLadderState & op2)const { return score < op2.score; };
  bool operator == (wordLadderState & op2)const { return score == op2.score; };
};

std::ostream& operator<<(std::ostream& ss, wordLadderState & gs);

#endif
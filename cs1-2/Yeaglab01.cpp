//Jake Yeagley
//CS 1410
//lab 1
//this lab will take a sentence and count the number of words

#include <iostream>
#include <cstring>

using namespace std;

const int SIZE = 80;						//constant varible

char* getString();							//prototype for scope
int countWords(char* sentence);
//this function takes the two other functions and runs the output
int main()
{
	char *pointer;							//pointer varible
	int ans = 0;

	pointer = getString();					//running input function
	ans = countWords(pointer);				//running counter function
	
	cout << "you have " << ans << " words in your sentence" << endl;		//outputting
	
	delete [] pointer;						//deleting pointer

	return 0;
}
//this function gets the input of the sentence
char* getString()
{
	int length;
	char got[SIZE];
												//getting input
	cout << "please enter a sentence that is less than 80 characters." << endl;
	cin.getline(got,SIZE);
	
	length = strlen(got);						//creating the size
	length = length + 1;						//acounting for null
	
	char *ptr;									//pointer for dynamically allocated memory
	ptr = new char[length];

	strcpy(ptr, got);							//copies the sentence to the pointer

	return ptr;
}
//this counts the words
int countWords(char* sentence)
{		
	int num = 1;								//varibles
	int length = strlen(sentence);
		for(int i=0; i<length; i++)				//counter 
		{
			if(*(sentence + i) == ' ')
				num++;
		}

	num = num + 1;								//acounts for last word

	return num;
}


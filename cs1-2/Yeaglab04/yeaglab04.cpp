#include "yeagRational.h"					//including class files		
#include <iostream>
//this program takes two number for a fraction and reduces them 
using namespace std;

int main()
{
	Rational fraction;
	int numerator, denominator;				//variables to store input numerator and denominator
											//inputing 
	cout << "this program takes two number for a fraction and reduces them" << endl;
	cout << "please enter a number for the numerator" << endl;
	cin >> numerator;
	cout << "please enter a number for the denominator" << endl;
	cin >> denominator;

	fraction.setNumerator(numerator);		//calling set functions
	fraction.setDenominator(denominator);

	fraction.setReduced();					//reducing the fraction

	cout << "your fraction is ";			//outputting 
	fraction.printReduced();
	cout << endl;

	return 0;

}
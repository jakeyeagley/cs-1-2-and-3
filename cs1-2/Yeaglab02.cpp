//Jake Yeagley
//CS 1410
//lab 2
//this function recieves a fraction and reduces it to it's lowest form

#include <iostream>
	
struct fraction
	{
		int top;										//global structure
		int bottom;
	};

using namespace std;
	
fraction getFraction();									//function prototypes
fraction reduceFraction(fraction nonReduced);
void displayFraction(fraction answer);

int main()
{
	fraction NRF,RF;									//prompting
	cout << "this funciton will ask you to input a function, then reduce it to it's lowest form" 
		<< endl;
	NRF = getFraction();								//calling functions
	RF = reduceFraction(NRF);
	displayFraction(RF);

	return 0;
}

fraction getFraction()
{
	fraction f;
														//inputing top/bottom fraction
	cout << "please input the top part of the fraction" << endl;
	cin >> f.top;
	cout << "please input the bottom part of the fraciton" << endl;
	cin >> f.bottom;

	return f;
}

fraction reduceFraction(fraction nonReduced)
{
	fraction reduced;									//setting up limit for for loop and greatest common factor variable
	int limit, GCF;

	if(nonReduced.top < nonReduced.bottom)				//finding the GCF
		limit = nonReduced.top;
	else
		limit = nonReduced.bottom;

	for(int i = limit;i > 0; i--)
	{
		if (nonReduced.bottom % i == 0 && nonReduced.top % i == 0)
			{
				GCF = i;
				break;
			}
	}
														//just in case you wanted to know the GCF
	cout << "your greatest common factor is " << GCF << endl;

	reduced.top = nonReduced.top / GCF;					//divide top and bottom by GCF
	reduced.bottom = nonReduced.bottom / GCF;
	
	return reduced;
}

void displayFraction(fraction answer)
{														//dispay reduced fraction
	cout << "your reduced fraction is " << answer.top << "/" << answer.bottom << endl;
}

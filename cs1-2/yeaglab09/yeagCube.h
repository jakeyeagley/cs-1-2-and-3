//Jake Yeagley, Lab 09, CS 1415
#ifndef CUBE_H					//including the safe guards
#define CUBE_H

#include <iostream>
#include "yeagSquare.h"			//including the square 

using namespace std;


namespace Yeagley				//adding to the namespace
{
	class cube: public square
	{
	private:					//no private members they all come from inheratance
	public:
								//constructor with default values
		cube(double side = 0);
		double getArea()const;	//get functions there are no set functions
		double getVolume()const;
	};
}
#endif

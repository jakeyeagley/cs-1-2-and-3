//Jake Yeagley, Lab 10, CS 1415
#include <iostream>
#include "yeagRectangle.h"							//including the rectangle .h

using namespace std;

namespace Yeagley									//adding to the namespace
{
	rectangle::rectangle(double l, double w)		//constructor
	{
		length_ = l;
		width_ = w;
	}
	void rectangle::setLength(double length)		//set functions
	{
		if(length < 0.0)							//checking for a negative
			length_ = 0.0;
		else 
			length_ = length;
	}
	void rectangle::setWidth(double width)
	{
		if(width < 0.0)								//checking for a negative
			width_ = 0.0;
		else 
			width_ = width;
	}
	double rectangle::getLength()const				//get functions
	{
		return length_;
	}
	double rectangle::getArea()const
	{
		return length_ * width_;
	}

	double rectangle::getPerimeter()const
	{
		return 2 * length_ + 2 * width_;
	}
	void rectangle::print()const					//print function
	{
		cout << "rectangle with a length of " << length_ << " and a width of " << width_ << endl;
	}
}

//Jake Yeagley
//CS 1415
//lab 3
//This program asks for a color and gives you the light wavelength of that color

#include <iostream>														//librarys
#include <string>										
#include <cstring>

using namespace std;

enum Color {RED, BLUE, GREEN, YELLOW, ORANGE, PURPLE, NUM_COLORS};		//global datatype Color


Color getColor();
string displayColor(Color c);											//prototype functions
float wavelengthNum(Color num);	

int main()
{
	char yorn;															//yorn mean yes or no
	Color c;														
	float waveValue;
	string color; 

	cout << "This program asks for a color and gives you the light wavelength of that color."
		<< endl;
	do{
																		//prompting
	cout << "please enter a color." << endl;

	c = getColor();
	waveValue = wavelengthNum(c);
	color = displayColor(c);
																		//outputting 
	cout << "the wave length of " << color << " is " << waveValue << "nm" << endl;
	cout << "would you like to see another colors wavelength?" << endl;
	cout << "if yes type y, if no type n" << endl;
	cin >> yorn;

	}while(yorn == 'y');												//repeating on yes

	return 0;
}
Color getColor()
{
	Color x;
	string choice;														//variables
	
	cin >> choice;
																		//comparing strings
	while(choice.compare("red") != 0 && choice.compare("blue") != 0 && choice.compare("yellow") != 0 && 
		choice.compare("orange") != 0 && choice.compare("purple") != 0 && choice.compare("green") != 0)
	{
		cout << "invalid color" << endl;								//valadation
		cout << "enter a new color" << endl;
		cin >> choice;
	}

	if (choice == "red")												//converting to enum
		x = RED;
	else if (choice == "blue")
		x = BLUE;
	else if (choice == "green")
		x = GREEN;
	else if (choice == "yellow")
		x = YELLOW;
	else if (choice == "orange")
		x = ORANGE;
	else if (choice == "purple")
		x = PURPLE;

	return x;
}
string displayColor(Color c)
{
	string color;														//setting up color 
	switch(c)
	{
	case RED:
		color = "red";
		break;
	case BLUE:
		color = "blue";
		break;
	case GREEN:
		color = "green";
		break;
	case YELLOW:
		color = "yellow";
		break;
	case ORANGE:
		color = "orange";
		break;
	case PURPLE:
		color = "purple";
		break;
	}

	return color;
}
float wavelengthNum(Color c)
{
	float wave;
	
	if (c == RED)														//finding wavelength value
		wave = 632;
	else if (c == BLUE)
		wave = 466;
	else if (c == YELLOW)
		wave = 563;
	else if (c == ORANGE)
		wave = 475;
	else if (c == PURPLE)
		wave = 414;
	else if (c == GREEN)
		wave = 536;

	return wave;

}
//Jake yeagley
//CS 1415
//this program uses a class template to store an array
#ifndef YEAGARRAY_H							//including safeguards
#define YEAGARRAY_H
#include <iostream>							//including liraries
#include <exception>
#include <new>

using namespace std;

namespace Yeagley							//creating namespace Yeagley
{											//a class for throwing
	class arrayException: public exception
	{
	public:
		arrayException(char message[])		//derived from the exception class
			:exception(message)
		{}
	};

	template<typename t>					//class template
	class Array
	{
	private:
		t *ptr;								//pointer variable
		int size_;							//to hold the size of the array

	public:
		~Array();							//destructor
		Array(int);							//constructor
		int getSize()const;					//get size function
		void add(t);						//add function
		void remove();						//remove function
		t& at(int);							//at function
	};

	template<typename t>					//defining the class template's member functions
	Array<t>::Array(int size)				//constructor
	{
		if(size <= 0)						//if size is to small
		{
			throw arrayException("error with the memory");
		}
		
		try									//catching an error if pointer doesn't work
		{
		ptr = new t[size];
		}
		catch(bad_alloc)
		{
			throw arrayException("error with the memory");
		};	
		
		size_ = size;						//setting size
			
	}

	template<typename t>
	Array<t>::~Array()						//destructor
	{
		if(size_ > 0)						
			delete[] ptr;
	}

	template<typename t>
	int Array<t>::getSize()const
	{
		return size_;
	}

	template<typename t>
	void Array<t>::add(t value)				//add function
	{
		t *newptr;							//creating a new pointer
		
		newptr = ptr;						//pointing the new pointer to the old one
		
		size_ = size_ + 1;					//adding more memory
		
		ptr = new t[size_];					//pointing to the new size
		
		for(int i = 0; i < size_-1; i++)	//coping the data over
			ptr[i] = newptr[i];

		delete [] newptr;					//deleting "new pointer"

		ptr[size_ - 1] = value;				
	}
	
	template<typename t>
	void Array<t>::remove()					//remove function
	{
		t *newptr;							//creating a new pointer

		newptr = ptr;						//and having it point to the old pointer

		size_ = size_ - 1;					//removing the memory

		ptr = new t[size_];					//pointing to the new size

		for(int i = 0; i < size_; i++)		//coping data over
			ptr[i] = newptr[i];
		
		delete [] newptr;					//deleting the "new pointer"
	}

	template<typename t>					//at function
	t & Array<t>::at(int element)
	{										//checing input
		if (element < 0 || element >= size_ )
			throw arrayException("error");
		else							
			return ptr[element];			//returning the element
	}	
}
#endif YEAGARRAY_H


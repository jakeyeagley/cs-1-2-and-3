//Jake Yeagley lab11 CS 1415 
//this program shows how to use throws and catches as error checkers
#include <iostream>							//including libraries
#include <new>
#include <string>
#include "yeagexceptions.h"					//including my .h file

using namespace std;						//including my namespace
using namespace Yeagley;

void arithmeticOverflow();					//function prototyping
void outOfBounds();
void memoryAllocation();

int main()
{
											//prompting
	cout << "this program shows how to use different exception handling" << endl;
	int menu;								//menu variable
	do										//loop for using menu 
	{
		try									//try block
		{
											//prompting for menu
		cout << "which exception would you like to try" << endl;
		cout << "type 1 for overflow error, 2 for out of bounds error, and 3 for memory error" 
			<< endl;
		cin >> menu;
		if(menu == 1)						//type 1 for arithmeticOverflow function
			arithmeticOverflow();
		else if(menu == 2)					//type 2 for outOfBounds function
			outOfBounds();
		else if(menu == 3)					//type 3 for memoryAllocation function
			memoryAllocation();
		}
		catch(overFlowException e)			//catch block for over flow error
		{
			cout << e.what() << endl;
		}
		catch(subscriptException e)			//catch block for subscript error
		{
			cout << e.what() << endl;
		}
		catch(memoryException e)			//catch block for memory error
		{
			cout << e.what() << endl;
		}
											//type 1 to do again 0 to end program
		cout << "would you like to do it again? " << endl;
		cout << "type 0 for no and 1 for yes" << endl;
		cin >> menu;

	}while(menu != 0);

	return 0;
}

void arithmeticOverflow()					//overflow function
{
	int num = 2147483640;					//start with a number close to limit

	while(true)								//cause the error
	{
		cout << num << endl;
		if(num + 1 > num)
			num++;
		else
											//throw the error
			throw overFlowException ("Error: overflow");
	}

}

void outOfBounds()							//out of bounds function
{
	int const SIZE = 7;						//creating an array to be overfilled
	int error[SIZE];

	for(int i = 0;i < 10;i++)				//causing the error
	{
		if(i > SIZE)
			throw subscriptException("Error: subscript");
		else
			cout << error[i] << endl;
	}
}

void memoryAllocation()						//memory allocation function
{
	int SIZE = 1;							//creating a pointer
	int *ptr;

	try										//try block for bad_alloc
	{
		while (true)						//causing the error
		{
			ptr = new int[SIZE];
			delete [] ptr;
			SIZE = SIZE * 10;
		}
		
	}
	catch (bad_alloc e)						//catching the bad_alloc
	{										//throwing the error
		throw memoryException("Error: Memory");
	}
}
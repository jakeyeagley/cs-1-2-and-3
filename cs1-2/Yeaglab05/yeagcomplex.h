#ifndef Complex_H
#define Complex_H

namespace Yeagley
{

	class Complex
	{
	private:
		double real_;
		double imaginary_;

	public:
		Complex();
		void setReal(double);
		void setImaginary(double);
		double getReal() const;
		double getImaginary() const;
		Complex addComplex(Complex);
		Complex subComplex(Complex);
		void print();
	};
}

#endif
